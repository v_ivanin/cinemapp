package helper;

import java.io.IOException;
import java.util.Properties;

public class PropertyHolder {

    private static PropertyHolder propertyHolder;     // Variable that contains created object to use singleton pattern

    private boolean inMemoryDB;
    private String jdbcURL;
    private String dbUserLogin;
    private String dbUserPassword;
    private String dbDriver;

    /*
    Getters
     */

    public boolean isInMemoryDB() {
        return inMemoryDB;
    }

    public String getJdbcURL() {
        return jdbcURL;
    }

    public String getDbUserLogin() {
        return dbUserLogin;
    }

    public String getDbUserPassword() {
        return dbUserPassword;
    }

    public String getDbDriver() {
        return dbDriver;
    }

    /*
    Constructor
     */

    private PropertyHolder() {
        loadProperties();
    }

    /*
    Singleton
     */

    public static synchronized PropertyHolder getInstance() {
        if (propertyHolder == null) {
            propertyHolder = new PropertyHolder();
        }
        return propertyHolder;
    }

    /*
    Load properties from file in ClassLoader
     */

    private void loadProperties() {
        Properties properties = new Properties();

        try {
            properties.load(PropertyHolder.class.getClassLoader().getResourceAsStream("application.properties"));

            this.inMemoryDB = Boolean.valueOf(properties.getProperty("inMemoryDB"));
            this.jdbcURL = properties.getProperty("jdbcUrl");
            this.dbDriver = properties.getProperty("dbDriver");
            this.dbUserLogin = properties.getProperty("dbUserLogin");
            this.dbUserPassword = properties.getProperty("dbUserPassword");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}