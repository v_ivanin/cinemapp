package dao;

import dao.api.Dao;
import dao.impl.*;
import helper.PropertyHolder;
import model.*;

public class DaoFactory {

    /*
    Var for singleton
     */

    private static DaoFactory instance = null;

    /*
    Dao's fields to use
     */

    private Dao<Integer, Film> filmDao;
    private Dao<Integer, User> userDao;
    private Dao<Integer, Role> roleDao;
    private Dao<Integer, Hall> hallDao;
    private Dao<Integer, Session> sessionDao;
    private Dao<Integer, Ticket> ticketDao;

    /*
    Constructor
     */

    private DaoFactory() {
        loadDaos();
    }

    /*
    Singleton
     */

    public static DaoFactory getInstance() {

        if (instance == null) {
            instance = new DaoFactory();
        }
        return instance;
    }

    /*
    Loads Dao's to fields
     */

    private void loadDaos() {
        if (PropertyHolder.getInstance().isInMemoryDB()) {
            //NOR
        }
        else {
            setFilmDao(FilmDaoImpl.getInstance());
            setUserDao(UserDaoImpl.getInstance());
            setRoleDao(RoleDaoImpl.getInstance());
            setHallDao(HallDaoImpl.getInstance());
            setSessionDao(SessionDaoImpl.getInstance());
            setTicketDao(TicketDaoImpl.getInstance());
        }
    }

    /*
    Getters and setters
     */

    public Dao<Integer, Film> getFilmDao() {
        return filmDao;
    }

    public void setFilmDao(Dao<Integer, Film> filmDao) {
        this.filmDao = filmDao;
    }

    public Dao<Integer, User> getUserDao() {
        return userDao;
    }

    public void setUserDao(Dao<Integer, User> userDao) {
        this.userDao = userDao;
    }

    public Dao<Integer, Hall> getHallDao() {
        return hallDao;
    }

    public void setHallDao(Dao<Integer, Hall> hallDao) {
        this.hallDao = hallDao;
    }

    public Dao<Integer, Session> getSessionDao() {
        return sessionDao;
    }

    public void setSessionDao(Dao<Integer, Session> sessionDao) {
        this.sessionDao = sessionDao;
    }

    public Dao<Integer, Ticket> getTicketDao() {
        return ticketDao;
    }

    public void setTicketDao(Dao<Integer, Ticket> ticketDao) {
        this.ticketDao = ticketDao;
    }

    public Dao<Integer, Role> getRoleDao() {
        return roleDao;
    }

    public void setRoleDao(Dao<Integer, Role> roleDao) {
        this.roleDao = roleDao;
    }
}
