package dao.impl;

import dao.DaoFactory;
import dao.api.Dao;
import model.Session;
import model.Ticket;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class TicketDaoImpl extends CrudDAO<Ticket> {

    private static Dao<Integer, Session> sessionDao = DaoFactory.getInstance().getSessionDao();

    private static Dao<Integer, User> userDao = DaoFactory.getInstance().getUserDao();

    private static final String SQL_UPDATE = "UPDATE session SET row = ?, place = ?, sessionId = ?, " +
            "userId = ?, empty = ? WHERE id = ?";

    private static final String SQL_INSERT = "INSERT INTO ticket (row, place, sessionId, userId, empty)" +
            " values (?,?,?,?,?)";

    private static TicketDaoImpl ticketDao;

    public static TicketDaoImpl getInstance() {
        if (ticketDao == null) {
            ticketDao = new TicketDaoImpl(Ticket.class);
        }
        return ticketDao;
    }

    public TicketDaoImpl(Class<Ticket> type) {
        super(type);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement updateStatement = connection.prepareStatement(SQL_UPDATE);
        updateStatement.setInt(1, entity.getRow());
        updateStatement.setInt(2, entity.getPlace());
        updateStatement.setInt(3, entity.getSession().getId());
        updateStatement.setInt(4, entity.getUser().getId());
        updateStatement.setBoolean(5, entity.isEmpty());
        updateStatement.setInt(6, entity.getId());
        return updateStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Ticket entity) throws SQLException {
        PreparedStatement insertStatement = connection.prepareStatement(SQL_INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
        insertStatement.setInt(1, entity.getRow());
        insertStatement.setInt(2, entity.getPlace());
        insertStatement.setInt(3, entity.getSession().getId());
        insertStatement.setInt(4, entity.getUser().getId());
        insertStatement.setBoolean(5, entity.isEmpty());
        return insertStatement;
    }

    @Override
    protected List<Ticket> readAll(ResultSet resultSet) throws SQLException {
        List<Ticket> tickets = new LinkedList<>();
        Ticket ticket = null;
        while (resultSet.next()) {
            ticket = new Ticket();
            ticket.setId(resultSet.getInt("id"));
            ticket.setRow(resultSet.getInt("row"));
            ticket.setPlace(resultSet.getInt("place"));
            ticket.setSession(sessionDao.getById(resultSet.getInt("sessionId")));
            ticket.setUser(userDao.getById(resultSet.getInt("userId")));
            ticket.setEmpty(resultSet.getBoolean("empty"));
            tickets.add(ticket);
        }
        return tickets;
    }
}
