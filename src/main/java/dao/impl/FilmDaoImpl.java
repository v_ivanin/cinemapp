package dao.impl;

import model.Film;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class FilmDaoImpl extends CrudDAO<Film> {

    /*
    SQL scripts
     */

    private static final String SQL_UPDATE = "UPDATE film SET nameOfFilm = ?, description = ?, genre = ?, " +
            "yearOfProduction = ? WHERE id = ?";

    private static final String SQL_INSERT = "INSERT INTO film (nameOfFilm, description, genre, yearOfProduction) values (?,?,?,?)";

    /*
    Var for singleton
     */

    private static FilmDaoImpl crudDao;

    /*
    Constructor
     */

    private FilmDaoImpl(Class<Film> type) {
        super(type);
    }

    /*
    Singleton
     */

    public static synchronized FilmDaoImpl getInstance() {
        if (crudDao == null) {
            crudDao = new FilmDaoImpl(Film.class);
        }
        return crudDao;
    }

    /*
    Methods
     */

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Film entity) throws SQLException {
        PreparedStatement insertStatement = connection.prepareStatement(SQL_INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
        insertStatement.setString(1, entity.getNameOfFilm());
        insertStatement.setString(2, entity.getDescription());
        insertStatement.setString(3, entity.getGenre());
        insertStatement.setInt(4, entity.getYearOfProduction());
        return insertStatement;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Film entity) throws SQLException {
        PreparedStatement updateStatement = connection.prepareStatement(SQL_UPDATE);
        updateStatement.setString(1, entity.getNameOfFilm());
        updateStatement.setString(2, entity.getDescription());
        updateStatement.setString(3, entity.getGenre());
        updateStatement.setInt(4, entity.getYearOfProduction());
        updateStatement.setInt(5, entity.getId());
        return updateStatement;
    }


    @Override
    protected List<Film> readAll(ResultSet resultSet) throws SQLException {
        List<Film> filmList = new LinkedList<>();
        Film film = null;
        while (resultSet.next()) {
            film = new Film();
            film.setId(resultSet.getInt("id"));
            film.setNameOfFilm(resultSet.getString("nameOfFilm"));
            film.setDescription(resultSet.getString("description"));
            film.setGenre(resultSet.getString("genre"));
            film.setYearOfProduction(resultSet.getInt("yearOfProduction"));
            filmList.add(film);
        }
        return filmList;
        }
}
