package dao.impl;

import model.Hall;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class HallDaoImpl extends CrudDAO<Hall> {

    /*
    SQL scripts
     */

    private static final String SQL_UPDATE = "UPDATE hall SET nameOfHall = ?, quantityOfRows = ?, quantityOfPlacesInRows = ?, " +
            "pricePerSeat = ? WHERE id = ?";

    private static final String SQL_INSERT = "INSERT INTO hall (nameOfHall, quantityOfRows, quantityOfPlacesInRows, pricePerSeat)" +
            " values (?,?,?,?)";

    /*
    Var for singleton
     */

    private static HallDaoImpl hallDao;

    /*
    Singleton
     */

    public static synchronized HallDaoImpl getInstance() {
        if (hallDao == null) {
            hallDao = new HallDaoImpl(Hall.class);
        }
        return hallDao;
    }

    /*
    Constructor
     */

    private HallDaoImpl(Class<Hall> type) {
        super(type);
    }

    /*
    Methods
     */

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement insertStatement = connection.prepareStatement(SQL_INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
        insertStatement.setString(1, entity.getNameOfHall());
        insertStatement.setInt(2, entity.getQuantityOfRows());
        insertStatement.setInt(3, entity.getQuantityOfPlacesInRows());
        insertStatement.setInt(4, entity.getPricePerSeat());
        return insertStatement;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Hall entity) throws SQLException {
        PreparedStatement updateStatement = connection.prepareStatement(SQL_UPDATE);
        updateStatement.setString(1, entity.getNameOfHall());
        updateStatement.setInt(2, entity.getQuantityOfRows());
        updateStatement.setInt(3, entity.getQuantityOfPlacesInRows());
        updateStatement.setInt(4, entity.getPricePerSeat());
        updateStatement.setInt(5, entity.getId());
        return updateStatement;
    }

    @Override
    protected List<Hall> readAll(ResultSet resultSet) throws SQLException {
        List<Hall> halls = new LinkedList<>();
        Hall hall = null;
        while (resultSet.next()) {
            hall = new Hall();
            hall.setId(resultSet.getInt("id"));
            hall.setNameOfHall(resultSet.getString("nameOfHall"));
            hall.setQuantityOfRows(resultSet.getInt("quantityOfRows"));
            hall.setQuantityOfPlacesInRows(resultSet.getInt("quantityOfPlacesInRows"));
            hall.setPricePerSeat(resultSet.getInt("pricePerSeat"));
            halls.add(hall);
        }
        return halls;
    }
}
