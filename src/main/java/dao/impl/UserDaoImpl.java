package dao.impl;

import dao.DaoFactory;
import dao.api.Dao;
import model.Role;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class UserDaoImpl extends CrudDAO<User> {

    private static Dao<Integer, Role> roleDao = DaoFactory.getInstance().getRoleDao();

    /*
    SQL scripts
     */

    private static final String SQL_UPDATE = "UPDATE user SET firstName = ?, lastName = ?, email = ?, " +
            "password = ?, roleId = ? WHERE id = ?";

    private static final String SQL_INSERT = "INSERT INTO user (firstName, lastName, email, password, roleId) values (?,?,?,?,?)";

    /*
    Var for singleton
     */

    private static UserDaoImpl userDao;

    /*
    Singleton
     */

    public static UserDaoImpl getInstance() {
        if (userDao == null) {
            userDao = new UserDaoImpl(User.class);
        }
        return userDao;
    }

    /*
    Constructor
     */

    private UserDaoImpl(Class<User> type) {
        super(type);
    }

    /*
    Methods
     */

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement insertStatement = connection.prepareStatement(SQL_INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
        insertStatement.setString(1, entity.getFirstName());
        insertStatement.setString(2, entity.getLastName());
        insertStatement.setString(3, entity.getEmail());
        insertStatement.setString(4, entity.getPassword());
        insertStatement.setInt(5, entity.getRole().getId());
        return insertStatement;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, User entity) throws SQLException {
        PreparedStatement updateStatement = connection.prepareStatement(SQL_UPDATE);
        updateStatement.setString(1, entity.getFirstName());
        updateStatement.setString(2, entity.getLastName());
        updateStatement.setString(3, entity.getEmail());
        updateStatement.setString(4, entity.getPassword());
        updateStatement.setInt(5, entity.getRole().getId());
        updateStatement.setInt(6, entity.getId());
        return updateStatement;
    }

    @Override
    protected List<User> readAll(ResultSet resultSet) throws SQLException {
        List<User> userList = new LinkedList<>();
        User user = null;
        while (resultSet.next()) {
            user = new User();
            user.setId(resultSet.getInt("id"));
            user.setFirstName(resultSet.getString("firstName"));
            user.setLastName(resultSet.getString("lastName"));
            user.setEmail(resultSet.getString("email"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(roleDao.getById(resultSet.getInt("roleId")));
            userList.add(user);
        }
        return userList;
    }
}
