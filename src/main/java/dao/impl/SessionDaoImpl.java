package dao.impl;

import dao.DaoFactory;
import dao.api.Dao;
import model.Film;
import model.Hall;
import model.Session;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class SessionDaoImpl extends CrudDAO<Session> {

    /*
    Dao's static fields to get objects of Hall and User
     */

    private static Dao<Integer, Hall> hallDao = DaoFactory.getInstance().getHallDao();

    private static Dao<Integer, Film> filmDao = DaoFactory.getInstance().getFilmDao();

    /*
    SQL scripts
     */

    private static final String SQL_UPDATE = "UPDATE session SET date = ?, time = ?, hallId = ?, " +
            "filmId = ?, ended = ? WHERE id = ?";

    private static final String SQL_INSERT = "INSERT INTO session (date, time, hallId, filmId, ended)" +
            " values (?,?,?,?,?)";

    /*
    Var for singleton
     */

    private static SessionDaoImpl sessionDao;

    /*
    Singleton
     */

    public static SessionDaoImpl getInstance() {
        if (sessionDao==null) {
            sessionDao = new SessionDaoImpl(Session.class);
        }
        return sessionDao;
    }

    /*
    Constructor
     */

    private SessionDaoImpl(Class<Session> type) {
        super(type);
    }

    /*
    Methods
     */

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Session entity) throws SQLException {
        PreparedStatement insertStatement = connection.prepareStatement(SQL_INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
        insertStatement.setDate(1, Date.valueOf(entity.getDate()));
        insertStatement.setTime(2, Time.valueOf(entity.getTime()));
        insertStatement.setInt(3, entity.getHall().getId());
        insertStatement.setInt(4, entity.getFilm().getId());
        insertStatement.setBoolean(5, entity.isEnded());
        return insertStatement;
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Session entity) throws SQLException {
        PreparedStatement updateStatement = connection.prepareStatement(SQL_UPDATE);
        updateStatement.setDate(1, Date.valueOf(entity.getDate()));
        updateStatement.setTime(2, Time.valueOf(entity.getTime()));
        updateStatement.setInt(3, entity.getHall().getId());
        updateStatement.setInt(4, entity.getFilm().getId());
        updateStatement.setBoolean(5, entity.isEnded());
        updateStatement.setInt(6, entity.getId());
        return updateStatement;
    }

    @Override
    protected List<Session> readAll(ResultSet resultSet) throws SQLException {
        List<Session> sessions = new LinkedList<>();
        Session session = null;
        while (resultSet.next()) {
            session = new Session();
            session.setId(resultSet.getInt("id"));
            session.setDate(resultSet.getDate("date").toLocalDate());
            session.setTime(resultSet.getTime("time").toLocalTime());
            session.setHall(hallDao.getById(resultSet.getInt("hallId")));
            session.setFilm(filmDao.getById(resultSet.getInt("filmId")));
            session.setEnded(resultSet.getBoolean("ended"));
            sessions.add(session);
        }
        return sessions;
    }
}
