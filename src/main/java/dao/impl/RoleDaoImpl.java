package dao.impl;

import model.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class RoleDaoImpl extends CrudDAO<Role> {

    private static final String SQL_UPDATE = "UPDATE role SET roleName = ? WHERE id = ?";

    private static final String SQL_INSERT = "INSERT INTO role (roleName) values (?)";

    private static RoleDaoImpl roleDao;

    public static RoleDaoImpl getInstance() {
        if (roleDao == null) {
            roleDao = new RoleDaoImpl(Role.class);
        }
        return roleDao;
    }

    private RoleDaoImpl(Class<Role> type) {
        super(type);
    }

    @Override
    protected PreparedStatement createUpdateStatement(Connection connection, Role entity) throws SQLException {
        PreparedStatement updateStatement = connection.prepareStatement(SQL_UPDATE);
        updateStatement.setString(1, entity.getRoleName());
        updateStatement.setInt(2, entity.getId());
        return updateStatement;
    }

    @Override
    protected PreparedStatement createInsertStatement(Connection connection, Role entity) throws SQLException {
        PreparedStatement insertStatement = connection.prepareStatement(SQL_INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
        insertStatement.setString(1, entity.getRoleName());
        return insertStatement;
    }

    @Override
    protected List<Role> readAll(ResultSet resultSet) throws SQLException {
        List<Role> roles = new LinkedList<>();
        Role role = null;
        while (resultSet.next()) {
            role = new Role();
            role.setId(resultSet.getInt("id"));
            role.setRoleName(resultSet.getString("roleName"));
            roles.add(role);
        }
        return roles;
    }
}
