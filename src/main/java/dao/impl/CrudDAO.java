package dao.impl;

import dao.api.Dao;
import datasource.DataSource;
import model.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public abstract class CrudDAO <T extends Entity<Integer>> implements Dao<Integer, T> {

    private Class<T> type;
    private DataSource dataSource;

    public static final String SELECT_ALL = "SELECT * FROM %s";
    public static final String GET_BY = "SELECT * FROM %s WHERE %s = ?";
    public static final String FIND_BY_ID = "SELECT * FROM %s WHERE id = ?";
    public static final String DELETE_BY_ID = "DELETE FROM %s WHERE id = ?";

    public CrudDAO(Class<T> type) {
        this.type = type;
        dataSource = DataSource.getInstance();
    }

    public List<T> getAll() {

        String sql = String.format(SELECT_ALL, type.getSimpleName());
        List<T> result = null;
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()) {
                result = readAll(resultSet);
        } catch (SQLException e ) {
            e.printStackTrace();
        }

        return result;
    }

    public T getById(Integer key) {
        String sql = String.format(FIND_BY_ID, type.getSimpleName());
        Connection connection = dataSource.getConnection();
        List<T> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, key);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result.get(0);
    }

    public List<T> getAllBy(String fieldName, String value) {
        String sql = String.format(GET_BY, type.getSimpleName(), fieldName);
        Connection connection = dataSource.getConnection();
        List<T> result = null;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, value);
            ResultSet resultSet = preparedStatement.executeQuery();
            result = readAll(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public T getBy(String fieldName, String value) {
        List<T> list = getAllBy(fieldName, value);
        if (list.size() > 0) {
            return list.get(0);
        } else {
            return null;
        }
    }

    public void save(T entity) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = createInsertStatement(connection, entity)) {
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                entity.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(T entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = createUpdateStatement(connection, entity)){

            preparedStatement.executeUpdate();

        }catch (SQLException e ) {
            e.printStackTrace();
        }
    }

    public void delete(Integer key) {

        String sql = String.format(DELETE_BY_ID, type.getSimpleName());

        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setInt(1,key);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected abstract PreparedStatement createUpdateStatement (Connection connection, T entity) throws SQLException;

    protected abstract PreparedStatement createInsertStatement (Connection connection, T entity) throws SQLException;

    protected abstract List<T> readAll(ResultSet resultSet) throws SQLException;
}
