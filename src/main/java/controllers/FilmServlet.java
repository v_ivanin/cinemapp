package controllers;

import dto.FilmDTO;
import dto.SessionDTO;
import service.impl.FilmServiceImpl;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "FilmServlet", urlPatterns = "/film")
public class FilmServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FilmDTO filmDTO = FilmServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("id")));
        request.setAttribute("filmDTO", filmDTO);
        List<SessionDTO> sessionDTOs = SessionServiceImpl.getInstance().getByFilmId(filmDTO.getId().toString());
        request.setAttribute("sessionDTOs", sessionDTOs);
        request.getRequestDispatcher("/pages/common/film.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
