package controllers;

import dto.UserDTO;
import service.impl.RoleServiceImpl;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@WebServlet(name = "RegisterServlet", urlPatterns = "/register")
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        if (email!=null) {
            if (!validateEmail(email)) {
                request.getSession().setAttribute("message", "E-mail должен соответствовать шаблону example@hostname.domain");
                response.sendRedirect(request.getContextPath() + "/pages/common/register_result.jsp");
            }
            else if (UserServiceImpl.getInstance().getByEmail(email) != null) {
                request.getSession().setAttribute("message", "Пользователь с данным email уже существует");
                response.sendRedirect(request.getContextPath() + "/pages/common/register_result.jsp");
            }
            else {
                UserDTO userDTO = new UserDTO(firstName,lastName,email,password, RoleServiceImpl.getInstance().getById(2));
                UserServiceImpl.getInstance().save(userDTO);

                request.getSession().setAttribute("message", "Регистрация успешна");
                response.sendRedirect(request.getContextPath() + "/pages/common/register_result.jsp");
            }
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    private boolean validateEmail(String email) {
        Pattern pattern = Pattern.compile(".*?@.*?\\..*");
        Matcher matcher = pattern.matcher(email);
        if (matcher.find()) {
            return true;
        }
        else {
            return false;
        }
    }
}
