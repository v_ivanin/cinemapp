package controllers;

import dto.UserDTO;
import service.impl.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "LoginServlet", urlPatterns = "/login")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        UserDTO userDTO = UserServiceImpl.getInstance().getByEmail(email);

        if (userDTO != null && userDTO.getPassword().equals(password)) {
            request.getSession().setAttribute("user", userDTO);
            if (request.getSession().getAttribute("url") == null) {
                response.sendRedirect("/cinemapp");
            }
            else {
                response.sendRedirect(request.getSession().getAttribute("url").toString());
            }
        }
        else {
            request.getSession().setAttribute("message", "Wrong name or password");
            response.sendRedirect(request.getContextPath() + "/pages/common/login.jsp");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
