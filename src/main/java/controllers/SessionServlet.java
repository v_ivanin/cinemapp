package controllers;

import dto.FilmDTO;
import dto.SessionDTO;
import dto.TicketDTO;
import service.impl.FilmServiceImpl;
import service.impl.SessionServiceImpl;
import service.impl.TicketServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;

@WebServlet(name = "SessionServlet", urlPatterns = "/session")
public class SessionServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SessionDTO sessionDTO = SessionServiceImpl.getInstance().getById(Integer.parseInt(request.getParameter("id")));
        String time = sessionDTO.getTime().format(DateTimeFormatter.ofPattern("HH':'mm"));
        request.setAttribute("time", time);
        String date = sessionDTO.getDate().format(DateTimeFormatter.ofPattern("dd'.'MM"));
        request.setAttribute("date", date);
        FilmDTO filmDTO = FilmServiceImpl.getInstance().getById(sessionDTO.getFilm().getId());
        request.setAttribute("filmDTO", filmDTO);
        List<TicketDTO> ticketDTOs = TicketServiceImpl.getInstance().getBySessionId(sessionDTO.getId().toString());
        request.setAttribute("ticketDTOs", ticketDTOs);
        request.getRequestDispatcher("pages/common/session.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
