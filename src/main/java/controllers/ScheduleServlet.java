package controllers;

import dto.SessionDTO;
import service.impl.SessionServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@WebServlet(name = "ScheduleServlet", urlPatterns = "/schedule")
public class ScheduleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<LocalDate> localDateList = new LinkedList<>();
        List<String> dates = new LinkedList<>();
        Map<String, List<SessionDTO>> stringListMap = new LinkedHashMap<>();

        for (int i = 0; i < 7; i++) {
            LocalDate date = LocalDate.now().plusDays(i);
            localDateList.add(date);
            List<SessionDTO> sessionDTOs = SessionServiceImpl.getInstance().getByDate(date);
            String dateString = date.format(DateTimeFormatter.ofPattern("EEEE', 'dd'.'MM"));
            dates.add(dateString);
            stringListMap.put(dateString, sessionDTOs);
        }

        request.setAttribute("dateStartString", dates.get(0));
        request.setAttribute("dateEndString", dates.get(dates.size()-1));
        request.setAttribute("stringListMap", stringListMap);

        request.getRequestDispatcher("pages/common/schedule.jsp").forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
