package model;

import java.time.LocalDate;
import java.time.LocalTime;

public class Session extends Entity<Integer> {

    /*
    Fields
     */

    private LocalDate date;

    private LocalTime time;

    private Hall hall;

    private Film film;

    private boolean ended;

    /*
    Getters and setters
     */

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public boolean isEnded() {
        return ended;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }

    /*
    Constructors
     */

    public Session() {

    }

    public Session(LocalDate date, LocalTime time, Hall hall, Film film, boolean ended) {
        this.date = date;
        this.time = time;
        this.hall = hall;
        this.film = film;
        this.ended = ended;
    }

    /*
    Equals and hashcode
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Session session = (Session) o;

        if (ended != session.ended) return false;
        if (date != null ? !date.equals(session.date) : session.date != null) return false;
        if (time != null ? !time.equals(session.time) : session.time != null) return false;
        if (hall != null ? !hall.equals(session.hall) : session.hall != null) return false;
        return film != null ? film.equals(session.film) : session.film == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        result = 31 * result + (film != null ? film.hashCode() : 0);
        result = 31 * result + (ended ? 1 : 0);
        return result;
    }

    /*
    toString()
     */

    @Override
    public String toString() {
        return "Session{" +
                ", sessionID " +
                ", date=" + date +
                ", time=" + time +
                ", hall=" + hall +
                ", film=" + film +
                ", ended=" + ended +
                '}';
    }
}
