package model;

public class Film extends Entity<Integer>{

    /*
    Fields
     */

    private String nameOfFilm;

    private String description;

    private String genre;

    private int yearOfProduction;

    /*
    Getters and setters
     */

    public String getNameOfFilm() {
        return nameOfFilm;
    }

    public void setNameOfFilm(String nameOfFilm) {
        this.nameOfFilm = nameOfFilm;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    /*
    Constructors
     */

    public Film() {

    }

    public Film(String nameOfFilm, String description, String genre, int yearOfProduction) {
        setNameOfFilm(nameOfFilm);
        setDescription(description);
        setGenre(genre);
        setYearOfProduction(yearOfProduction);
    }

    /*
    Equals and hashcode
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Film film = (Film) o;

        if (yearOfProduction != film.yearOfProduction) return false;
        if (nameOfFilm != null ? !nameOfFilm.equals(film.nameOfFilm) : film.nameOfFilm != null) return false;
        if (description != null ? !description.equals(film.description) : film.description != null) return false;
        return genre != null ? genre.equals(film.genre) : film.genre == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nameOfFilm != null ? nameOfFilm.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + yearOfProduction;
        return result;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return "FilmDTO{" +
                "filmId = " + getId() +
                ", nameOfFilm='" + nameOfFilm + '\'' +
                ", description='" + description + '\'' +
                ", genre='" + genre + '\'' +
                ", yearOfProduction=" + yearOfProduction +
                '}';
    }
}
