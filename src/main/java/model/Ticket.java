package model;

public class Ticket extends Entity<Integer>{

    /*
    Fields
     */

    private int row;

    private int place;

    private Session session;

    private User user;

    private boolean empty;

    /*
    Getters and setters
     */

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    /*
    Constructor
     */

    public Ticket() {

    }

    public Ticket(int row, int place, Session session, User user, boolean empty) {
        this.row = row;
        this.place = place;
        this.session = session;
        this.user = user;
        this.empty = empty;
    }

    /*
    Equals and hashcode
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Ticket ticket = (Ticket) o;

        if (row != ticket.row) return false;
        if (place != ticket.place) return false;
        if (empty != ticket.empty) return false;
        if (session != null ? !session.equals(ticket.session) : ticket.session != null) return false;
        return user != null ? user.equals(ticket.user) : ticket.user == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + row;
        result = 31 * result + place;
        result = 31 * result + (session != null ? session.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (empty ? 1 : 0);
        return result;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return "Ticket{" +
                "ticketId= " + this.getId() +
                ", row=" + row +
                ", place=" + place +
                ", session=" + session +
                ", user=" + user +
                ", empty=" + empty +
                '}';
    }
}
