package dto;

import model.Entity;

public class FilmDTO extends Entity<Integer> {

    /*
    Fields
     */

    private String nameOfFilm;

    private String description;

    private String genre;

    private int yearOfProduction;

    /*
    Getters and setters
     */

    public String getNameOfFilm() {
        return nameOfFilm;
    }

    public void setNameOfFilm(String nameOfFilm) {
        this.nameOfFilm = nameOfFilm;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    /*
    Constructors
     */

    public FilmDTO() {

    }

    public FilmDTO(String nameOfFilm, String description, String genre, int yearOfProduction) {
        setNameOfFilm(nameOfFilm);
        setDescription(description);
        setGenre(genre);
        setYearOfProduction(yearOfProduction);
    }

    /*
    Equals and hashcode
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        FilmDTO filmDTO = (FilmDTO) o;

        if (yearOfProduction != filmDTO.yearOfProduction) return false;
        if (nameOfFilm != null ? !nameOfFilm.equals(filmDTO.nameOfFilm) : filmDTO.nameOfFilm != null) return false;
        if (description != null ? !description.equals(filmDTO.description) : filmDTO.description != null) return false;
        return genre != null ? genre.equals(filmDTO.genre) : filmDTO.genre == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nameOfFilm != null ? nameOfFilm.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + yearOfProduction;
        return result;
    }

    /*
    toString()
     */

    @Override
    public String toString() {
        return "FilmDTO{" +
                "filmId = " + getId() +
                ", nameOfFilm='" + nameOfFilm + '\'' +
                ", description='" + description + '\'' +
                ", genre='" + genre + '\'' +
                ", yearOfProduction=" + yearOfProduction +
                '}';
    }
}
