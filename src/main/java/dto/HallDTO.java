package dto;

import model.Entity;

public class HallDTO extends Entity<Integer> {

    /*
    Fields
     */

    private String nameOfHall;

    private int quantityOfRows;

    private int quantityOfPlacesInRows;

    private int pricePerSeat;

    /*
    Getters and setters
     */

    public String getNameOfHall() {
        return nameOfHall;
    }

    public void setNameOfHall(String nameOfHall) {
        this.nameOfHall = nameOfHall;
    }

    public int getQuantityOfRows() {
        return quantityOfRows;
    }

    public void setQuantityOfRows(int quantityOfRows) {
        this.quantityOfRows = quantityOfRows;
    }

    public int getQuantityOfPlacesInRows() {
        return quantityOfPlacesInRows;
    }

    public void setQuantityOfPlacesInRows(int quantityOfPlacesInRows) {
        this.quantityOfPlacesInRows = quantityOfPlacesInRows;
    }

    public int getPricePerSeat() {
        return pricePerSeat;
    }

    public void setPricePerSeat(int pricePerSeat) {
        this.pricePerSeat = pricePerSeat;
    }

    /*
    Constructors
     */

    public HallDTO () {

    }

    public HallDTO(String nameOfHall, int quantityOfRows, int quantityOfPlacesInRows, int pricePerSeat) {
        setNameOfHall(nameOfHall);
        setQuantityOfRows(quantityOfRows);
        setQuantityOfPlacesInRows(quantityOfPlacesInRows);
        setPricePerSeat(pricePerSeat);
    }

    /*
    Equals and hashcode
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        HallDTO hallDTO = (HallDTO) o;

        if (quantityOfRows != hallDTO.quantityOfRows) return false;
        if (quantityOfPlacesInRows != hallDTO.quantityOfPlacesInRows) return false;
        if (pricePerSeat != hallDTO.pricePerSeat) return false;
        return nameOfHall != null ? nameOfHall.equals(hallDTO.nameOfHall) : hallDTO.nameOfHall == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (nameOfHall != null ? nameOfHall.hashCode() : 0);
        result = 31 * result + quantityOfRows;
        result = 31 * result + quantityOfPlacesInRows;
        result = 31 * result + pricePerSeat;
        return result;
    }

    /*
    toString()
     */

    @Override
    public String toString() {
        return "HallDTO{" +
                " hallId =" + this.getId() +
                ", nameOfHall='" + this.getNameOfHall() + '\'' +
                ", quantityOfRows=" + this.getQuantityOfRows() +
                ", quantityOfPlacesInRows=" + this.getQuantityOfPlacesInRows() +
                ", pricePerSeat=" + this.getPricePerSeat() +
                '}';
    }
}
