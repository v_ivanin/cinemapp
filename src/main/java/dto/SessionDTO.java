package dto;

import model.Entity;

import java.time.LocalDate;
import java.time.LocalTime;

public class SessionDTO extends Entity<Integer> {

    /*
    Fields
     */
    private LocalDate date;

    private LocalTime time;

    private HallDTO hall;

    private FilmDTO film;

    private boolean ended;

    /*
    Getters and setters
     */

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public HallDTO getHall() {
        return hall;
    }

    public void setHall(HallDTO hall) {
        this.hall = hall;
    }

    public FilmDTO getFilm() {
        return film;
    }

    public void setFilm(FilmDTO film) {
        this.film = film;
    }

    public boolean isEnded() {
        return ended;
    }

    public void setEnded(boolean ended) {
        this.ended = ended;
    }

    /*
    Constructor
     */

    public SessionDTO() {

    }

    public SessionDTO(LocalDate date, LocalTime time, HallDTO hall, FilmDTO film, boolean ended) {
        this.date = date;
        this.time = time;
        this.hall = hall;
        this.film = film;
        this.ended = ended;
    }

    /*
    Equals and hashcode
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SessionDTO that = (SessionDTO) o;

        if (ended != that.ended) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (time != null ? !time.equals(that.time) : that.time != null) return false;
        if (hall != null ? !hall.equals(that.hall) : that.hall != null) return false;
        return film != null ? film.equals(that.film) : that.film == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (hall != null ? hall.hashCode() : 0);
        result = 31 * result + (film != null ? film.hashCode() : 0);
        result = 31 * result + (ended ? 1 : 0);
        return result;
    }

    /*
    toString()
     */

    @Override
    public String toString() {
        return "SessionDTO{" +
                "sessionId= " + this.getId() +
                ", date=" + date +
                ", time=" + time +
                ", hall=" + hall +
                ", film=" + film +
                ", ended=" + ended +
                '}';
    }
}
