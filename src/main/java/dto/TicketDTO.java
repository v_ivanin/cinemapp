package dto;

import model.Entity;


public class TicketDTO extends Entity<Integer> {

    /*
    Fields
     */

    private int row;

    private int place;

    private SessionDTO session;

    private UserDTO user;

    private boolean empty;

    /*
    Getters and setters
     */

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public SessionDTO getSession() {
        return session;
    }

    public void setSession(SessionDTO session) {
        this.session = session;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    /*
    Constructors
     */

    public TicketDTO() {

    }

    public TicketDTO(int row, int place, SessionDTO session, UserDTO user, boolean empty) {
        this.row = row;
        this.place = place;
        this.session = session;
        this.user = user;
        this.empty = empty;
    }

    /*
    Equals and hashcode
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        TicketDTO ticketDTO = (TicketDTO) o;

        if (row != ticketDTO.row) return false;
        if (place != ticketDTO.place) return false;
        if (empty != ticketDTO.empty) return false;
        if (session != null ? !session.equals(ticketDTO.session) : ticketDTO.session != null) return false;
        return user != null ? user.equals(ticketDTO.user) : ticketDTO.user == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + row;
        result = 31 * result + place;
        result = 31 * result + (session != null ? session.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (empty ? 1 : 0);
        return result;
    }

    /*
    toString
     */

    @Override
    public String toString() {
        return "TicketDTO{" +
                "ticketId=" + this.getId() +
                ", row=" + row +
                ", place=" + place +
                ", session=" + session +
                ", user=" + user +
                ", empty=" + empty +
                '}';
    }
}
