package dto;

import model.Entity;

public class RoleDTO extends Entity<Integer> {

    private String roleName;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public RoleDTO() {

    }

    public RoleDTO(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleDTO roleDTO = (RoleDTO) o;

        return roleName != null ? roleName.equals(roleDTO.roleName) : roleDTO.roleName == null;

    }

    @Override
    public int hashCode() {
        return roleName != null ? roleName.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "RoleDTO{" +
                "roleId " + getId() +
                "roleName='" + roleName + '\'' +
                '}';
    }
}
