package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.TicketDTO;
import mapper.BeanMapper;
import model.Ticket;
import service.api.Service;

import java.util.List;

public class TicketServiceImpl implements Service<Integer, TicketDTO> {

    private static TicketServiceImpl ticketService;
    private Dao<Integer, Ticket> ticketDao;

    public static TicketServiceImpl getInstance() {
        if (ticketService == null) {
            ticketService = new TicketServiceImpl();
        }
        return ticketService;
    }

    private TicketServiceImpl( ) {
        this.ticketDao = DaoFactory.getInstance().getTicketDao();
    }

    @Override
    public List<TicketDTO> getAll() {
        List<Ticket> ticketList = ticketDao.getAll();
        List<TicketDTO> ticketDTOs = BeanMapper.listMapToList(ticketList, TicketDTO.class);
        return ticketDTOs;
    }

    @Override
    public TicketDTO getById(Integer key) {
        Ticket ticket = ticketDao.getById(key);
        TicketDTO ticketDTO = BeanMapper.singleMapper(ticket, TicketDTO.class);
        return ticketDTO;
    }

    @Override
    public void save(TicketDTO entity) {
        Ticket ticket = BeanMapper.singleMapper(entity, Ticket.class);
        ticketDao.save(ticket);
    }

    @Override
    public void delete(Integer key) {
        ticketDao.delete(key);
    }

    @Override
    public void update(TicketDTO entity) {
        Ticket ticket = BeanMapper.singleMapper(entity, Ticket.class);
        ticketDao.update(ticket);
    }

    public List<TicketDTO> getBySessionId (String sessionId) {
        List<Ticket> tickets = ticketDao.getAllBy("sessionId", sessionId);
        List<TicketDTO> ticketDTOs = BeanMapper.listMapToList(tickets, TicketDTO.class);
        return ticketDTOs;
    }
}
