package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.RoleDTO;
import mapper.BeanMapper;
import model.Role;
import service.api.Service;
import java.util.List;

public class RoleServiceImpl implements Service<Integer, RoleDTO> {

    private static RoleServiceImpl roleService;
    private Dao<Integer, Role> roleDao;

    private RoleServiceImpl() {
        roleDao = DaoFactory.getInstance().getRoleDao();
    }

    public static RoleServiceImpl getInstance() {
        if (roleService == null) {
            roleService = new RoleServiceImpl();
        }
        return roleService;
    }

    @Override
    public List<RoleDTO> getAll() {
        List<Role> roles = roleDao.getAll();
        List<RoleDTO> roleDTOs = BeanMapper.listMapToList(roles, RoleDTO.class);
        return roleDTOs;
    }

    @Override
    public RoleDTO getById(Integer key) {
        Role role = roleDao.getById(key);
        RoleDTO roleDTO = BeanMapper.singleMapper(role, RoleDTO.class);
        return roleDTO;
    }

    @Override
    public void save(RoleDTO entity) {
        Role role = BeanMapper.singleMapper(entity, Role.class);
        roleDao.save(role);
    }

    @Override
    public void delete(Integer key) {
        roleDao.delete(key);
    }

    @Override
    public void update(RoleDTO entity) {
        Role role = BeanMapper.singleMapper(entity, Role.class);
        roleDao.update(role);
    }
}