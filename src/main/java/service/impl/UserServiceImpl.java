package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.UserDTO;
import mapper.BeanMapper;
import model.User;
import service.api.Service;

import java.util.List;

public class UserServiceImpl implements Service<Integer, UserDTO> {

    private static UserServiceImpl userService;
    private Dao<Integer, User> userDao;

    private UserServiceImpl() {
        userDao = DaoFactory.getInstance().getUserDao();
    }

    public static UserServiceImpl getInstance() {
        if (userService == null) {
            userService = new UserServiceImpl();
        }
        return userService;
    }

    @Override
    public List<UserDTO> getAll() {
        List<User> users = userDao.getAll();
        List<UserDTO> userDTOs = BeanMapper.listMapToList(users, UserDTO.class);
        return userDTOs;
    }

    @Override
    public UserDTO getById(Integer key) {
        User user = userDao.getById(key);
        UserDTO userDTO = BeanMapper.singleMapper(user, UserDTO.class);
        return userDTO;
    }

    @Override
    public void save(UserDTO entity) {
        User user = BeanMapper.singleMapper(entity, User.class);
        userDao.save(user);
    }

    @Override
    public void delete(Integer key) {
        userDao.delete(key);
    }

    @Override
    public void update(UserDTO entity) {
        User user = BeanMapper.singleMapper(entity, User.class);
        userDao.update(user);
    }

    public UserDTO getByEmail (String email) {
        User user = userDao.getBy("email", email);
        UserDTO userDTO = BeanMapper.singleMapper(user, UserDTO.class);
        return userDTO;
    }
}