package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.SessionDTO;
import mapper.BeanMapper;
import model.Session;
import service.api.Service;

import java.time.LocalDate;
import java.util.List;

public class SessionServiceImpl implements Service<Integer, SessionDTO> {

    private static SessionServiceImpl sessionService;
    private Dao<Integer, Session> sessionDao;

    private SessionServiceImpl() {
        sessionDao = DaoFactory.getInstance().getSessionDao();
    }

    public static SessionServiceImpl getInstance() {
        if (sessionService == null) {
            sessionService = new SessionServiceImpl();
        }
        return sessionService;
    }

    @Override
    public List<SessionDTO> getAll() {
        List<Session> sessions = sessionDao.getAll();
        List<SessionDTO> sessionDTOs = BeanMapper.listMapToList(sessions, SessionDTO.class);
        return sessionDTOs;
    }

    @Override
    public SessionDTO getById(Integer key) {
        Session session = sessionDao.getById(key);
        SessionDTO sessionDTO = BeanMapper.singleMapper(session, SessionDTO.class);
        return sessionDTO;
    }

    @Override
    public void save(SessionDTO entity) {
        Session session = BeanMapper.singleMapper(entity, Session.class);
        sessionDao.save(session);
    }

    @Override
    public void delete(Integer key) {
        sessionDao.delete(key);
    }

    @Override
    public void update(SessionDTO entity) {
        Session session = BeanMapper.singleMapper(entity, Session.class);
        sessionDao.update(session);
    }

    public List<SessionDTO> getByFilmId (String filmId) {
        List<Session> sessions = sessionDao.getAllBy("filmId", filmId);
        List<SessionDTO> sessionDTOs = BeanMapper.listMapToList(sessions, SessionDTO.class);
        return sessionDTOs;
    }

    public List<SessionDTO> getByDate (LocalDate localDate) {
        List<Session> sessions = sessionDao.getAllBy("date", localDate.toString());
        List<SessionDTO> sessionDTOs = BeanMapper.listMapToList(sessions, SessionDTO.class);
        return sessionDTOs;
    }
}
