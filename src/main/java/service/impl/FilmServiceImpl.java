package service.impl;

import dao.DaoFactory;
import dao.api.Dao;
import dto.FilmDTO;
import mapper.BeanMapper;
import model.Film;
import service.api.Service;

import java.util.List;

public class FilmServiceImpl implements Service<Integer, FilmDTO> {

    private static FilmServiceImpl filmService;
    private Dao<Integer, Film> filmDao;
    private BeanMapper beanMapper;

    private FilmServiceImpl() {
        filmDao = DaoFactory.getInstance().getFilmDao();
        beanMapper = BeanMapper.getInstance();
    }

    public static FilmServiceImpl getInstance() {
        if (filmService == null) {
            filmService = new FilmServiceImpl();
        }
        return filmService;
    }
    @Override
    public List<FilmDTO> getAll() {
        List<Film> films = filmDao.getAll();
        List<FilmDTO> filmDTOs = beanMapper.listMapToList(films, FilmDTO.class);
        return filmDTOs;
    }

    @Override
    public FilmDTO getById(Integer key) {
        Film film = filmDao.getById(key);
        FilmDTO filmDTO = beanMapper.singleMapper(film, FilmDTO.class);
        return filmDTO;
    }

    @Override
    public void save(FilmDTO entity) {
        Film film = BeanMapper.singleMapper(entity, Film.class);
        filmDao.save(film);
    }

    @Override
    public void delete(Integer key) {
        filmDao.delete(key);
    }

    @Override
    public void update(FilmDTO entity) {
        Film film = beanMapper.singleMapper(entity, Film.class);
        filmDao.update(film);
    }
}
