<%--
  Created by IntelliJ IDEA.
  User: Виталий
  Date: 19.01.2017
  Time: 16:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Расписание</title>
</head>
<body>
    <h3> Расписание сенсов: ${dateStartString} - ${dateEndString}</h3><br>
    <c:forEach items="${stringListMap}" var="entry">
        ${entry.key}<br>
        <c:forEach items="${entry.value}" var="session">
        ${session.time} - ${session.film.nameOfFilm} <br>
        </c:forEach>
    </c:forEach>
</body>
</html>
