<%--
  Created by IntelliJ IDEA.
  User: Виталий
  Date: 16.01.2017
  Time: 10:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Films</title>
</head>
<body>
<center>
    <c:forEach items="${filmDTOList}" var="film">
        <a href="${pageContext.servletContext.contextPath}/film?id=${film.id}">${film.nameOfFilm}</a><br/>
    </c:forEach>
</center>
</body>
</html>

